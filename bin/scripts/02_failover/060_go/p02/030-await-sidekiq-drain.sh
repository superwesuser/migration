#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
UNSYMLINKED_SCRIPT_DIR="$(greadlink -f "${SCRIPT_DIR}" || readlink "${SCRIPT_DIR}" || echo "${SCRIPT_DIR}")"
# shellcheck disable=SC1091,SC1090
source "${UNSYMLINKED_SCRIPT_DIR}/../../../../workflow-script-commons.sh"

# --------------------------------------------------------------

if [[ ${FAILOVER_ENVIRONMENT} == "stg" ]]; then
  DEPLOY_HOST=deploy.stg.gitlab.com
elif [[ ${FAILOVER_ENVIRONMENT} == "prd" ]]; then
  DEPLOY_HOST=deploy.gitlab.com
fi

# remote_gitlab_runner will log the command internally
remote_gitlab_runner "${DEPLOY_HOST}" "${SCRIPT_DIR}/030-await-sidekiq-drain.rb"
