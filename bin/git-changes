#!/usr/bin/env ruby

require 'fileutils'

# Encapsulate git changes
class Git
  REMOTE = 'origin'.freeze
  BRANCH = 'master'.freeze

  attr_reader :repo

  def initialize(repo)
    @repo = repo
  end

  def init
    FileUtils.mkdir_p(repo)
    git %w[init .]
  end

  def add_remote(url)
    git %W[remote add #{REMOTE} #{url}]
  end

  def pull
    git %W[pull #{REMOTE} #{BRANCH}]
  end

  def add(*files)
    git ['add'] + files
  end

  def commit(message)
    git %W[commit -m #{message}]
  end

  def push
    git %W[push #{REMOTE} #{BRANCH}]
  end

  private

  def git(*args)
    args = ['git', *args].flatten
    puts "#{repo} $ #{args.join(' ')}"
    Dir.chdir(repo) { Kernel.system(*args) }
  end
end

# Create a new git repository
class Setup
  attr_reader :git, :url

  def initialize(git, url)
    @git = git
    @url = url
  end

  def call
    git.init
    git.add_remote(url)
    git.pull
  end
end

# Loop around a block
class Looper
  attr_reader :delay, :n

  def initialize(delay, &blk)
    @delay = delay
    @blk = blk

    @n = 0
  end

  def call
    loop do
      once
      @n += 1
    end
  end

  private

  def once
    STDOUT.puts "\n# Loop #{n}"

    start = Time.now
    @blk.call(n)
    finish = Time.now

    duration = (finish - start).to_i
    waittime = [(delay - duration).to_i, 0].max

    puts "# Completed in #{duration} seconds, sleeping for #{waittime} seconds"
    sleep(waittime)
  end
end

# Push a change to the repository
class Changer
  attr_reader :git

  def initialize(git)
    @git = git
  end

  def call(n)
    system("dd if=/dev/urandom of=#{path(n)} bs=1024 count=1024 2>/dev/null")
    git.add(base(n))
    git.commit("Change #{n}")
    git.push
  end

  private

  def base(n)
    format('%04d.urandom', n)
  end

  def path(n)
    File.join(git.repo, base(n))
  end
end

def usage!(error)
  STDERR.puts <<-DOCS.gsub(/^  /, '')
  #{error}

  usage: #{$PROGRAM_NAME} setup <git-repo> <git-url>
         #{$PROGRAM_NAME} <push|pull> <git-repo>

  This script can be used to monitor Geo replication and instance accessibility.
  It requires a remote Git repository to operate against. Typical usage:

  #{$PROGRAM_NAME} setup primary git@<primary-url>:<repo>
  #{$PROGRAM_NAME} setup secondary git@<secondary-url>:<repo>

  Then in two separate terminals:

  #{$PROGRAM_NAME} push primary
  #{$PROGRAM_NAME} pull secondary

  Changes will be pushed to the primary and requested from the secondary every
  15 seconds. You can monitor the command output to track the reachability of
  each node, and any replication lag.
  DOCS

  exit 1
end

def arity(n)
  if ARGV.size == n + 1
    yield
  else
    usage!("Exactly #{n} arguments expected, got #{ARGV.size - 1}")
  end
end

git = Git.new(ARGV[1])

behaviour =
  case ARGV[0]
  when 'setup' then arity(2) { Setup.new(git, ARGV[2]) }
  when 'push'  then arity(1) { Looper.new(15) { |n| Changer.new(git).call(n) } }
  when 'pull'  then arity(1) { Looper.new(15) { git.pull } }
  else
    usage!("Unrecognized command: '#{ARGV[0]}'")
  end

behaviour.call
